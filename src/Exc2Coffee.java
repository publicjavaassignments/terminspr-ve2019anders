import java.util.ArrayList;

public class Exc2Coffee {
    public static void main(String[] args) {
        ArrayList<Product> products = new ArrayList<Product>();

        Juice juice1 = new Juice(5, "Appelsin");
        Juice juice2 = new Juice(3, "Æble");
        Juice juice3 = new Juice(10, "Guava");

        Coffee coffee1 = new Coffee(20, "God kaffe Inc.", "Afrika");
        Coffee coffee2 = new Coffee(25, "Bedre kaffe Inc.", "Majorka");
        Coffee coffee3 = new Coffee(30, "Bedste kaffe Inc.", "Sverige");

        products.add(juice1);
        products.add(juice2);
        products.add(juice3);
        products.add(coffee1);
        products.add(coffee2);
        products.add(coffee3);

        for (int i = 0; i < 6; i++) {
            System.out.println("Produkt " + (i + 1));
            System.out.println(products.get(i).description());
        }
    }

    public interface Product {
        double price();

        String description();
    }

    public static class Juice implements Product {
        private double price;
        private String sortOfFruit;

        public Juice(double price, String sortOfFruit) {
            this.price = price;
            this.sortOfFruit = sortOfFruit;
        }

        public double price() {
            return price;
        }

        @Override
        public String description() {
            return "Juice med " + sortOfFruit + " smag " + "til en pris på " + price + " dkkr.";
        }
    }

    public static class Coffee implements Product {
        private double price;
        private String brand;
        private String origin;

        public Coffee(double price, String brand, String origin) {
            this.price = price;
            this.brand = brand;
            this.origin = origin;
        }

        public double price() {
            return price;
        }

        @Override
        public String description() {
            return "Kaffe fra: " + brand + " importeret fra: " + origin + " til en pris på " + price + " dkkr.";
        }
    }
}
