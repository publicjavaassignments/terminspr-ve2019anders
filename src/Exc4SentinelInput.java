import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Exc4SentinelInput {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int numberOfInputs = 0;
        int lastInput = 0;
        double sum = 0;
        double finalSum = 0;
        List<Integer> inputInts = new ArrayList<Integer>();

        System.out.println("Please input some integers, type -1 to stop.");

        do {
            while (!keyboard.hasNextInt()) {
                String input = keyboard.next();
                System.out.printf("\"%s\" is not an integer!\n", input);
            }
            lastInput = keyboard.nextInt();
            if (lastInput != -1) {
                inputInts.add(lastInput);
                numberOfInputs++;
            }
            if (numberOfInputs < 3 && lastInput == -1) {
                System.out.println("You need to input more than 3 integers.");
            }
        } while (lastInput != -1 || numberOfInputs < 3);

        for (int i = 0; i < inputInts.size(); i++)
            sum += inputInts.get(i);

        // Negative integers may return odd numbers
        finalSum = (sum - (Collections.min(inputInts) + Collections.max(inputInts)));

        System.out.printf("The sum of your input was: %.2f", + finalSum);

    }
}
