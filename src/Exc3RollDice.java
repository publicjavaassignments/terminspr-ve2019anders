public class Exc3RollDice {
    public static void main(String[] args) {
        System.out.println("3.a) Dice rolled: " + rollDice());
        System.out.println("3.b) Final number of invokes to reach specified integer: " + countInvocations());
        System.out.println("3.c) The average of 100 dice throws yielded an average of: " + countRollAverage());
    }

    public static int rollDice() {
        int diceThrow = (int) (Math.random() * 6) + 1;
        return diceThrow;
    }

    public static int countInvocations() {
        int searchTerm = 3;
        int invocationCount = 0;
        int diceRoll = 0;

        while (diceRoll != searchTerm) {
            diceRoll = (rollDice());
            invocationCount++;
            //System.out.println("Dice roll returned " + diceRoll + " making the invocation count: " + invocationCount);
        }

        return invocationCount;

    }

    public static double countRollAverage() {
        double totalSum = 0;
        int[] countRollArray = new int[100];

        for (int i = 0; i < 100; i++) {
            int diceRoll = rollDice();
            countRollArray[i] = diceRoll;
        }

        for (int i = 0; i < countRollArray.length; i++) {
            totalSum = totalSum + countRollArray[i];
        }

        double average = (totalSum / countRollArray.length);

        return average;
    }


}
