import java.util.ArrayList;

public class Exc1Array {
    public static void main(String[] args) {
        ArrayList<String> carArray = new ArrayList<String>();
        carArray.add("Fiat");
        carArray.add("Ford");
        carArray.add("Ferrari");
        ArrayList<String> carArrayCopy = (ArrayList<String>) carArray.clone();
        System.out.println(carArrayCopy.toString());
    }
}
