public class Exc6Recursion {
    public static void main(String[] args) {
        //System.out.println("Using forloop:");
        //printDigitsOnRowForLoop(3);
        //System.out.println("Using recursion:");
        printDigitsOnRowRecursion(3);
    }

    public static void printDigitsOnRowForLoop(int digit) {
        int currentDigit = 0;

        if (digit > 9 || digit < 0) {
            System.out.println("Only ints between 0 and 9 are allowed");
        } else {
            for (int i = 0; i < digit + 1; i++) {
                for (int j = 0; j < 10; j++) {
                    System.out.printf(String.valueOf(currentDigit));
                }
                currentDigit++;
                System.out.printf("\n");
            }
        }
    }

    public static void printDigitsOnRowRecursion(int digit) {
        int currentDigit = digit;

        if (digit + 1 > 0) {
            // The for loop merely prints the defined int 10 times
            for (int j = 0; j < 10; j++) {
                System.out.printf(String.valueOf(currentDigit));
            }
            System.out.printf("\n");
            printDigitsOnRowRecursion(digit - 1);
        }
    }
}
