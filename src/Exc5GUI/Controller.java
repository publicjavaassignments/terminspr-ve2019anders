package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {

    public TextField codeInputSegment1, codeInputSegment2, codeInputSegment3, codeInputSegment4;
    public Button checkCodeButton;
    @FXML
    Label accessField;

    public void checkCodeButton() {
        int segment1 = Integer.parseInt(codeInputSegment1.getText());
        int segment2 = Integer.parseInt(codeInputSegment2.getText());
        int segment3 = Integer.parseInt(codeInputSegment3.getText());
        int segment4 = Integer.parseInt(codeInputSegment4.getText());

        if (segment1 == 5 & segment2 == 6 & segment3 == 7 & segment4 == 3) {
            accessField.setText("Okay");
        }
    }
}
